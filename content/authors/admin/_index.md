+++
# Display name
name = "Amit Sharma"

# Username (this should match the folder name)
authors = ["admin"]

# Is this the primary user of the site?
superuser = true

# Role/position
role = "Principal Researcher"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
organizations = [ { name = "Microsoft Research India", url = "https://www.microsoft.com/en-us/research/lab/microsoft-research-india/" } ]

# Short bio (displayed in user profile at end of posts)
bio = "Principal Researcher at Microsoft."

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = ""

# List (academic) interests or hobbies
interests = [    
    "Causal Inference",
    "Trustworthy Machine Learning",
    "Technology and Mental Health"
]

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
#user_groups = ["Researchers", "Visitors"]

# List qualifications (such as academic degrees)
[[education.courses]]
  course = "PhD in Computer Science"
  institution = "Cornell University"
  year = 2015

[[education.courses]]
  course = "B.Tech. in Computer Science"
  institution = "IIT Kharagpur"
  year = 2010

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.

[[social]]
  icon = "envelope"
  icon_pack = "fas"
  link = "#contact"  # For a direct email link, use "mailto:test@example.org".

[[social]]
  icon = "twitter"
  icon_pack = "fab"
  link = "https://twitter.com/amt_shrma"

[[social]]
  icon = "google-scholar"
  icon_pack = "ai"
  link = "https://scholar.google.com/citations?user=CXgQufgAAAAJ&hl=en"

[[social]]
  icon = "github"
  icon_pack = "fab"
  link = "https://github.com/amit-sharma"

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# [[social]]
#   icon = "cv"
#   icon_pack = "ai"
#   link = "files/cv.pdf"

+++

**2022.07.25**: Talk on necessity of causal inference for out-of-distribution generalization in prediction and decision-making at the Technion, Israel. [[Slides](files/causal-ml-dowhy-prediction-decisionmaking.pdf)]

**2022.05.31**: _DoWhy_ library for causal inference evolves to an independent py-why org to foster wider collaboration. Contributions welcome! [[Blog](https://www.microsoft.com/en-us/research/blog/dowhy-evolves-to-independent-pywhy-model-to-help-causal-inference-grow/)][[Github](https://github.com/py-why/dowhy)][[Arxiv](https://arxiv.org/abs/2011.04216)]

**2021.12.02**: Talk on Causal Inference for Machine Learning: Generalization,
Explanation and Fairness; at the UK Office for National Statistics. [[Slides](files/causality-for-machine-learning-econdatascience-seminar.pdf)] 

**2020.12.03**: [Emre](http://kiciman.org) and I gave a Microsoft Research
webinar on causal inference and its implications for machine learning. [[Video](https://note.microsoft.com/MSR-Webinar-DoWhy-Library-Registration-On-Demand.html)] 

**2020.08.13**: Featured on the [Humans of AI](https://humansofai.podbean.com/) podcast.
[[Apple Podcasts](https://podcasts.apple.com/au/podcast/emre-kiciman-amit-sharma-causal-inference-microsofts/id1464995550?i=1000487994771)][[Spotify](https://open.spotify.com/episode/6Rc3rZsAfcNGOQFXop7p0P)]

**2020.07.23**: Session on causal machine learning with [Elias Bareinboim](https://causalai.net/), [Susan Athey](https://athey.people.stanford.edu/), and [Cheng Zhang](https://cheng-zhang.org/). [[Video](https://www.microsoft.com/en-us/research/video/frontiers-in-machine-learning-big-ideas-in-causality-and-machine-learning/)]
 
**2020.06.30**: MatchDG: Learning causal predictive models that generalize
to new distributions [[Paper](https://arxiv.org/abs/2006.07500)][[Code](https://github.com/microsoft/robustdg)]

**2019.05.30**: DiCE: Using *counterfactual examples* to explain machine learning. [[Paper](http://arxiv.org/abs/1905.07697)][[Python Library](https://github.com/Microsoft/DiCE)][[Blog](https://www.microsoft.com/en-us/research/blog/open-source-library-provides-explanation-for-machine-learning-through-diverse-counterfactuals/)]

**2018.08.19**: [Emre](http://kiciman.org) and I gave a tutorial on causal inference at
[KDD](http://www.kdd.org/kdd2018/). [[Slides](https://causalinference.gitlab.io/kdd-tutorial)]



-----

Data tells stories. My research aims to tell the _causal_ story.

As machine learning systems move into societally critical domains such as healthcare, education, finance and criminal justice, questions on their impact gain fundamental importance. 
The key insight in my work is to consider modern algorithms as *interventions*, just like a medical treatment or an economic policy. Unlike typical interventions studied in social and biomedical sciences, however, algorithmic interventions can be arbitrarily complex. I work on developing methods to estimate causal impact of such systems and build algorithms that  optimize the causal effect. I am also passionate about designing new interventions for societal impact, especially in healthcare.

If you are interested in working with me at MSR India, drop me an email. We hire interns throughout the year. There are also [postdoctoral positions](https://www.microsoft.com/en-us/research/msr-india-hiring/) available. Additionally, 
if you are an undergraduate or a masters student, <!--you can additionally apply to the pre-doctoral [Research Fellowship](link-to-rf) program.-->
 our lab runs an excellent pre-doctoral [Research Fellowship](https://www.microsoft.com/en-us/research/lab/microsoft-research-india/research-fellow-program/)
program.

[**Read more...**]({{< ref "/page/research-summary.md" >}}) 
