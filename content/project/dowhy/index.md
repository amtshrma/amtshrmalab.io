+++
# Project title.
title = "DoWhy: A library for causal inference"

# Date this page was created.
date = 2018-04-27T00:00:00

# Project summary to display on homepage.
summary = """DoWhy provides a principled, end-to-end approach to causal inference. <br/>
[Blog post](link) | [Github](gf)"""

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["causal-inference"]

# Optional external URL for project (replaces project detail page).
external_link = "http://github.com/Microsoft/dowhy"

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = "Photo by Toa Heftiba on Unsplash"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"
+++
