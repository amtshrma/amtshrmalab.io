+++
# Project title.
title = "Causal inference tutorial"

# Date this page was created.
date = 2018-04-26T00:00:00

# Project summary to display on homepage.
summary = """If you are interested in causal inference, check out one of my tutorials from [IC2S2](f) or [KDD](kdd-link). <br/>
[Quick Intro](http://github.com/amit-sharma/causal-inference-tutorial) | [KDD
2018 Tutorial](http://causalinference.gitlab.io/kdd-tutorial/)"""

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["causal-inference"]

# Optional external URL for project (replaces project detail page).
external_link = "http://github.com/Microsoft/dowhy"

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = "Photo by Toa Heftiba on Unsplash"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"
+++
