---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Causality and Large Language Models: A new frontier"
event: "Leibniz AI Artificial Intelligence, Causality and Personalised Medicine Symposium"
event_url: "https://aicpm2023.de/"
location: "Hannover, Germany"
address:
  street:
  city:
  region:
  postcode:
  country:
summary:
abstract: "A key challenge in conducting causal analyses is that identifying the correct assumptions, such as the causal graph, needs considerable manual effort. Since a causal graph cannot be learnt from data alone, domain experts face the difficult task of providing underlying causal relationships and also verifying them. In this talk, I will discuss how large language models (LLMs) provide new capabilities that were so far understood to be restricted to domain experts, such as inferring the direction of causal relationships, identifying any missing relationships, or verifying the underlying assumptions in a causal analysis. First, I will present results on the causal graph discovery capabilities of LLMs. Algorithms based on GPT-3.5 and 4 outperform existing algorithms on a wide variety of datasets: Tubingen pairwise dataset (97%, 13 points gain) spanning domains such as physics, engineering, biology, and soil science; arctic sea ice coverage dataset (0.22 hamming distance, 11 points gain); and a medical pain diagnosis dataset. We find that LLMs infer causal relationships by relying on information such as variable names, a process that we call knowledge-based reasoning that is distinct from and complementary to non-LLM based causal discovery. Second, I will describe how these capabilities of LLMs can be extended for useful tasks in the causal inference pipeline: identifying any missing confounders, suggesting instrumental variables, suggesting special variables like negative control that can validate causal analyses, and reasoning about root cause attribution. At the same time, LLMs exhibit unpredictable failure modes and I will provide some techniques to interpret their robustness, especially with respect to dataset memorization. Looking ahead, by capturing domain knowledge about causal mechanisms, LLMs may open new frontiers for advancing causal inference research and enable the widespread adoption of causal methods."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2023-09-14T15:00:00
#date_end: 2020-12-25T17:14:46+05:30
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: 2023-09-15

authors: []
tags: [llm]

# Is this a featured talk? (true/false)
featured: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides: ""

url_code:
url_pdf:
url_video:

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

<iframe src="https://onedrive.live.com/embed?resid=fb9a18ae325d3efb%2112186&amp;authkey=!AIEjc9cdKWME8v4&amp;em=2&amp;wdAr=1.7777777777777777" width="476px" height="288px" frameborder="0">This is an embedded <a target="_blank" href="https://office.com">Microsoft Office</a> presentation, powered by <a target="_blank" href="https://office.com/webapps">Office</a>.</iframe>

