---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Teaching causal reasoning to language models"
event: "NeurIPS 2024 Causality and Large Models workshop"
event_url: "https://calm-workshop-2024.github.io/"
location: "Vancouver, Canada"
address:
  street:
  city:
  region:
  postcode:
  country:
summary:
abstract: "Large language models (LLMs) have demonstrated remarkable accuracy in identifying cause-and-effect relationships across diverse scientific domains. However, their ability to reason over these relationships remains a challenge. To address this, we propose axiomatic training—a novel approach that enhances causal reasoning by teaching LLMs fundamental causal axioms one at a time, rather than fine-tuning them for specific tasks. By training on synthetic demonstrations of axioms such as transitivity and d-separation, we show that models with fewer than 100 million parameters can surpass reasoning capabilities of significantly larger models such as Phi-3, Gemini Pro and GPT-4. Axiomatic training has practical applications as a tool for constructing verifiers for LLM-generated reasoning and for embedding inductive biases into LLM fine-tuning. Moreover, it provides insights into how models like GPT-4, trained solely on observational data, can exhibit advanced reasoning capabilities."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2024-14-12T15:00:00
#date_end: 2020-12-25T17:14:46+05:30
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: 2024-12-13

authors: []
tags: [llm]

# Is this a featured talk? (true/false)
featured: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides: "teaching-causal-reasoning-language-models.pdf"

url_code:
url_pdf: 
url_video:

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

<iframe src="https://onedrive.live.com/embed?resid=fb9a18ae325d3efb%2112186&amp;authkey=!AIEjc9cdKWME8v4&amp;em=2&amp;wdAr=1.7777777777777777" width="476px" height="288px" frameborder="0">This is an embedded <a target="_blank" href="https://office.com">Microsoft Office</a> presentation, powered by <a target="_blank" href="https://office.com/webapps">Office</a>.</iframe>

