+++
title = "The impact of computing systems | Causal inference in practice"
publishDate = 2019-07-28T00:00:00  # Schedule page publish date.
draft = false

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date = 2019-07-25T11:00:00
#time_end = 2030-06-01T15:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Amit Sharma"]

# Abstract and optional shortened version.
abstract = "Computing and machine learning systems are affecting almost all parts of our lives and the society at large. How do we formulate and estimate the impact of these systems? This talk introduces causal inference as a methodology to answer such questions and provides examples of applying it to estimating impact of recommender systems, online social media feeds, search engines and interventions in public health in India."
summary = ""

# Name of event and optional event URL.
event = "ACM Summer School on Human-centered AI"
event_url = "https://hcixb.org"

# Location of event.
location = "New Delhi, India"

# Is this a selected talk? (true/false)
selected = true

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
#projects = ["internal-project"]

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []

# Links (optional).
url_pdf = ""
url_slides = "https://www.slideshare.net/AmitSharma315/the-impact-of-computing-systems-causal-inference-in-practice"
url_video = ""
url_code = ""

# Does the content use math formatting?
math = false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  # caption = "Image credit: [**Unsplash**](https://unsplash.com/photos/bzdhc5b3Bxs)"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Right"
+++
