+++
title = "Research Summary"
date = "2018-09-15"
# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
# Use `caption` to display an image caption.
#   Markdown linking is allowed, e.g. `caption = "[Image credit](http://example.org)"`.
# Set `preview` to `false` to disable the thumbnail in listings.
[header]
image = ""
caption = ""
preview = true

+++

Data tells stories. My research aims to tell the _causal_ story.

As machine learning systems move into societally critical domains such as healthcare, education, finance and criminal justice, questions on their impact gain fundamental importance. 
The key insight in my work is to consider modern algorithms as *interventions*, just like a medical treatment or an economic policy. Unlike typical interventions studied in social and biomedical sciences, however, algorithmic interventions can be arbitrarily complex. I work on developing methods to estimate causal impact of such systems and build algorithms that  optimize the causal effect. I am also passionate about designing new interventions for societal impact, especially in healthcare.

Take recommender systems as an example. Algorithmic recommendations are ubiquitous in the online world, celebrated for providing roughly 30-80% of activity and yet criticized for restricting people's information access in social media to "filter bubbles". A natural question then is: How much do these systems _actually cause_ this behavior, and how much of it is just correlational? My research shows that these claims overstate the causal effect: e-commerce recommendation engines such as Amazon's account for [less than 10%](https://arxiv.org/abs/1611.09414) of the traffic, and recommendation systems that show friends' activities in music, movies, or books, account for a [tiny fraction of people's overall activity](https://arxiv.org/abs/1604.01105) in these domains.  

Two central questions in my research are:

* How do we correctly design algorithmic interventions, reason about generalizability, and plan for when they go wrong? 
* How do we design a new algorithmic intervention to have the desired effect?  

Most of my current work is on the first question, where I build on causal inference literature and develop methods that require minimal assumptions to estimate a causal effect. The second one is harder, and I look forward to a future where we can _predict_ causal effect of an algorithmic intervention. While there are ongoing efforts in machine learning and statistics to optimize algorithmic interventions through better evaluation criteria or data collection, a purely algorithmic approach just scratches the surface of what is really a sociological problem---the role of algorithms embedded with human decision-making in a societal context. Put differently, the fundamental problem remains a causal one with interacting effects of between societal context, algorithms and individuals.  

At Microsoft Research, I work on these problems with an amazing set of colleagues from the [Technology for Emerging Markets](https://www.microsoft.com/en-us/research/group/technology-for-emerging-markets/) and the [Algorithms and Data Sciences](https://www.microsoft.com/en-us/research/group/algorithms-and-data-sciences/) groups. From 2015-2017, I was a postdoctoral researcher at MSR New York working with [Jake Hofman](http://jakehofman.com/) and [Duncan Watts](https://www.microsoft.com/en-us/research/people/duncan/). I am fortunate to be advised by [Dan Cosley](https://www.cs.cornell.edu/~danco/) for my Ph.D. Before that, I studied computer science at [IIT Kharagpur](http://www.iitkgp.ac.in/).  

If you are interested in working with me at MSR India, drop me an email. We hire interns throughout the year. There are also [postdoctoral positions](https://www.microsoft.com/en-us/research/msr-india-hiring/) available.
