+++
title = "Causal Inference Resources"
date = "2019-02-11"
# Featured image
# Use `caption` to display an image caption.
#   Markdown linking is allowed, e.g. `caption = "[Image credit](http://example.org)"`.
# Set `preview` to `false` to disable the thumbnail in listings.
[header]
image = ""
caption = ""
preview = true

+++
## Tutorials

A breezy introduction to causal inference: 
[IC2S2](https://www.github.com/amit-sharma/causal-inference-tutorial/)

Advanced tutorial on causal inference: 
[KDD 2018](https://causalinference.gitlab.io/kdd-tutorial/)

## DoWhy Python library

Code: [DoWhy](https://www.github.com/Microsoft/DoWhy/)

Docs: [Documentation](https://causalinference.gitlab.io/dowhy/)

## Book

Causal Reasoning: Fundamental and Machine Learning Applications

[Book
Outline](https://causalinference.gitlab.io/Causal-Reasoning-Fundamentals-and-Machine-Learning-Applications/)

[All Chapters](https://causalinference.gitlab.io/)
