#!/usr/bin/env bash

# Simple steps:
# first, go to themes/academic, checkout the version of interest (here it is 4.6.3)
# then, just go back to root and say git add themes/academic and then say "git commit, git push". 
# Basically only checkout inside git submodule folder, do no other action
cd themes/academic
git checkout v4.6.3
#git submodule update --remote --merge
